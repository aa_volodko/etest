class StaticPagesController < ApplicationController
  def home
    if current_admin 
      @users = User.all
    end
  end

  def help
  end

end
