class CopleModesToAdmin < ActiveRecord::Migration
  def change
    add_column :admins, :god_mode, :boolean, default: false
    add_column :admins, :reports, :boolean, default: false
  end
end
