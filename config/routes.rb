Etest::Application.routes.draw do
  devise_for :users, :admins
  resources :users
  resources :admins
  root 'static_pages#home'
  match '/help', to: 'static_pages#help', via: 'get'

end
